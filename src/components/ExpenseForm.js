import React, { useState } from "react";
import styled from "styled-components";

const ExpenseForm = props => {
  const [enteredTitle, setEnteredTitle] = useState("");
  const [enteredAmount, setEnteredAmount] = useState("");
  const [enteredDate, setEnteredDate] = useState("");

  const titleChangeHandler = (event) => {
    setEnteredTitle(event.target.value);
  };

  const amountChangeHandler = (event) => {
    setEnteredAmount(event.target.value);
  };

  const dateChangeHandler = (event) => {
    setEnteredDate(event.target.value);
  };

  const submitHandler = (event) => {
    event.preventDefault();

    const expenseData = {
      title: enteredTitle,
      amount: enteredAmount,
      date: new Date(enteredDate),
    };
    props.onSaveExpenseData(expenseData);
    setEnteredTitle("");
    setEnteredAmount("");
    setEnteredDate("");
  };

  return (
    <form onSubmit={submitHandler}>
      <Newform>
        <Newform>
          <Label>Titulo</Label>
          <Input
            type="text"
            onChange={titleChangeHandler}
            value={enteredTitle}
          />
        </Newform>

        <Newform>
          <Label>Valor</Label>
          <Input
            type="number"
            min="0.01"
            step="0.01"
            onChange={amountChangeHandler}
            value={enteredAmount}
          />
        </Newform>

        <Newform>
          <Label>Data</Label>
          <Input
            type="date"
            min="01-01-2019"
            max="31-12-2022"
            onChange={dateChangeHandler}
            value={enteredDate}
          />
        </Newform>
      </Newform>
      <Actions>
        <ButtonOne type="button" onClick={props.onCancel}> Cancelar</ButtonOne>
        <Button type="submit">Add Despesa</Button>
      </Actions>
    </form>
  );
};

export default ExpenseForm;

const Newform = styled.div`
  display: block;
  flex-wrap: wrap;
  gap: 1rem;
  margin-bottom: 1rem;
  text-align: left;
`;

const Label = styled.label`
  font-weight: bold;
  margin-bottom: 0.5rem;
  display: block;
`;

const Input = styled.input`
  font: inherit;
  padding: 0.5rem;
  border-radius: 6px;
  border: 1px solid #ccc;
  width: 20rem;
  max-width: 100%;
`;

const Actions = styled.div`
  text-align: right;
`;

const Button = styled.button`
  font: inherit;
  cursor: pointer;
  padding: 1rem 1rem;
  border: 1px solid #40005d;
  background-color: #40005d;
  color: white;
  border-radius: 12px;
  margin-right: 1rem;
  &:hover {
    background-color: #6ECB63;
    border-color: #510674;
  }
`;

const ButtonOne = styled.button`
  font: inherit;
  cursor: pointer;
  padding: 1rem 1rem;
  border: 1px solid #40005d;
  background-color: #40005d;
  color: white;
  border-radius: 12px;
  margin-right: 1rem;
  &:hover {
    background-color: #B42B51;
    border-color: #510674;
  }
`;
