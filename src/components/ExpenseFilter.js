import React from "react";
import styled from "styled-components";

const ExpenseFilter = props => {
  const filterChangeHandler = (event) => {
  props.onFilterExpense(event.target.value);
};

  return (
    <Filter>
      <Control>
        <Label>Filtrar por Ano</Label>
        <Select value={props.selected} onChange={filterChangeHandler}>
          <option value="2022">2022</option>
          <option value="2021">2021</option>
          <option value="2020">2020</option>
          <option value="2019">2019</option>
        </Select>
      </Control>
    </Filter>
  );
};

export default ExpenseFilter;

const Filter = styled.div`
  color: white;
  padding: 0 1rem;
`;

const Control = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  margin: 1rem 0;
`;

const Label = styled.label`
  font-weight: bold;
  margin-bottom: 0.5rem;
`;

const Select = styled.select`
  font: inherit;
  padding: 0.5rem 3rem;
  font-weight: bold;
  border-radius: 6px;
`;
