import React, { useState } from "react";
import styled from "styled-components";

import ExpenseForm from "./ExpenseForm";

const NewExpense = props => {
  const [isEditing, setISEditing] = useState(false);

  const saveExpenseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    props.onAddExpense(expenseData);
    setISEditing(false);
  };

  const startEditing = () => {
    setISEditing(true);
  };

  const stopEditing = () => {
     setISEditing(false);
  };

  return (
    <New>
      {!isEditing && <Button onClick={startEditing}>Add Nova Despesa</Button>}
      {isEditing && <ExpenseForm onSaveExpenseData={saveExpenseDataHandler}
      onCancel={stopEditing} />}
    </New>
  );
};

export default NewExpense;

const New = styled.div`
  background-color: #a892ee;
  padding: 1rem;
  margin: 2rem auto;
  width: 50rem;
  max-width: 95%;
  border-radius: 12px;
  text-align: center;
  box-shadow: 0 1px 8px rgba(0, 0, 0, 0.25);
`;

const Button = styled.button`
  font: inherit;
  cursor: pointer;
  padding: 1rem 1rem;
  border: 1px solid #40005d;
  background-color: #40005d;
  color: white;
  border-radius: 12px;
  margin-right: 1rem;
  &:hover {
    background-color: #510674;
    border-color: #510674;
  }
`;


