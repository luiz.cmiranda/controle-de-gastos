import React, { useState } from "react";
import ExpenseItem from "./components/ExpenseItem";
import styled from "styled-components";

import NewExpense from "./components/NewExpense";
import ExpenseFilter from "./components/ExpenseFilter";
import ExpenseChart from "./components/ExpenseChart";

const INITIAL = [
  {
    id: "a1",
    title: "Toalha de Rosto",
    amount: 4.15,
    date: new Date(2020, 7, 14),
  },

  {
    id: "a2",
    title: "Tv Nova",
    amount: 1799.49,
    date: new Date(2021, 2, 12),
  },

  {
    id: "a3",
    title: "Seguro de Vida",
    amount: 25.55,
    date: new Date(2021, 2, 28),
  },

  {
    id: "a4",
    title: "Progressiva",
    amount: 139.65,
    date: new Date(2021, 5, 12),
  },
];

const App = () => {
  const [expenses, setExpenses] = useState(INITIAL);

  const addExpenserHandler = (expense) => {
    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
  };

  const [filterYear, setFilterYear] = useState("2021");

  const filterChange = (selectedYear) => {
    setFilterYear(selectedYear);
  };

  const filteredExpenses = expenses.filter((expense) => {
    return expense.date.getFullYear().toString() === filterYear;
  });

  let expensesContent = <P>Sem despesas neste ano.</P>;

  if (filteredExpenses.length > 0) {
    expensesContent = filteredExpenses.map((expense) => (
      <ExpenseItem
        key={expense.id}
        title={expense.title}
        amount={expense.amount}
        date={expense.date}
      />
    ));
  }

  return (
    <>
      <div>
        <NewExpense onAddExpense={addExpenserHandler} />
      </div>

      <Card>
        <ExpenseFilter selected={filterYear} onFilterExpense={filterChange} />
        <ExpenseChart expenses={filteredExpenses}/>
        {expensesContent}
      </Card>
    </>
  );
};

export default App;

const Card = styled.div`
  padding: 1rem;
  background-color: rgb(31, 31, 31);
  margin: 2rem auto;
  width: 50rem;
  max-width: 95%;
  border-radius: 12px;
`;

const P = styled.p`
  color: #ff0000;
  text-align: center;
`;
